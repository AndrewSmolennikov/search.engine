import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { SearchResult } from './search-result.model';

const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class SearchInputService {
    constructor(private http: HttpClient) {
    }

    getSearchResults(value: string): Observable<SearchResult[]> {
        return this.http.get<SearchResult[]>('/api/result/' + value);
    }

    getSearchResultsInDb(value: string): Observable<SearchResult[]> {
        return this.http.get<SearchResult[]>('api/getexist/' + value);
    }
}