export class SearchResult {

    constructor(
        public DisplayUrl: string,
        public Name: string,
        public CacheUrl: string
    ) { }
}