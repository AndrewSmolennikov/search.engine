import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { SearchInputService } from './search-input.service'
import { HttpClientModule } from '@angular/common/http';
import { SearchResultTable } from './search-result/search-result.cpomponent';
import { MatTableModule, MatDialogModule, MatToolbarModule, MatButtonModule } from '@angular/material';
import { NoopAnimationsModule, BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { CdkTableModule } from '@angular/cdk/table';
import { AlertComponent } from './alert/alert.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  exports: [
    MatButtonModule,
    MatDialogModule,
    MatToolbarModule
  ]
})
export class MaterialModule { }

@NgModule({
  declarations: [
    AppComponent,
    SearchResultTable,
    AlertComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    MatTableModule,
    MaterialModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    FormsModule
  ],
  entryComponents: [AlertComponent],
  exports: [MatTableModule, CdkTableModule],
  providers: [SearchInputService, SearchResultTable],
  bootstrap: [AppComponent]
})
export class AppModule { }
