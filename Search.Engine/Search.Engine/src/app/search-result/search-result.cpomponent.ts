import { Component, Injectable, OnInit, ViewChild, OnChanges, SimpleChange, SimpleChanges } from "@angular/core";
import { SearchInputService } from "../search-input.service";
import { DataSource } from "@angular/cdk/table";
import { Observable } from "rxjs";
import { MatTable, MatDialog } from "@angular/material";
import { SearchResult } from "../search-result.model";
import { AlertComponent } from "../alert/alert.component";

@Component({
    selector: 'result-table',
    styleUrls: ['search-result.component.css'],
    templateUrl: 'search-result.component.html',
})

@Injectable()
export class SearchResultTable implements OnInit, OnChanges {


    public displayedColumns: string[] = ['Name', 'CacheUrl'];
    @ViewChild('table') table: MatTable<SearchResult>;

    constructor(private searchInputService: SearchInputService, private dialog: MatDialog) {

    }

    ngOnInit() {
    }

    ngOnChanges(): void {
        this.table.renderRows();
    }

    getSearchResults(value: string) {
        if(value.trim().length == 0) {
            var dialog = this.dialog.open(AlertComponent, {
                width: '300px'
            });
        };
        this.searchInputService.getSearchResults(value).subscribe(data => this.table.dataSource = data);
        this.table.renderRows();
    }
}