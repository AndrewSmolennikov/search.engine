using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using AutoMapper;
using Search.Engine.BusinessLogic.Interfaces;
using Search.Engine.BusinessLogic.Models;
using Search.Engine.Models;

namespace Search.Engine.Controllers
{
  public class ResultController : ApiController
  {
    private readonly ISearchResultService _searchResultService;
    public ResultController(ISearchResultService searchResultService)
    {
      _searchResultService = searchResultService;
    }

    [Route("api/result/{value}")]
    public IEnumerable<SearchResultViewModel> Get(string value)
    {
      if (string.IsNullOrEmpty(value))
      {
        return Enumerable.Empty<SearchResultViewModel>();
      }

      var searchResult = _searchResultService.FindExist(value);

      var searchResultList = searchResult.ToList();

      if (searchResultList.Any())
      {
        return MapSearchResultViewModels(searchResultList);
      }

      searchResult = _searchResultService.GetResult(value);

      return MapSearchResultViewModels(searchResult);
    }

    private IEnumerable<SearchResultViewModel> MapSearchResultViewModels(IEnumerable<SearchResultDTO> resultValues)
    {
      var mapper = new MapperConfiguration(cfg => cfg.CreateMap<SearchResultDTO, SearchResultViewModel>()).CreateMapper();
      var results = mapper.Map<IEnumerable<SearchResultDTO>, List<SearchResultViewModel>>(resultValues);
      return results;
    }
  }
}
