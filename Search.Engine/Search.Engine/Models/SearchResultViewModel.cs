﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Search.Engine.Models
{
    public class SearchResultViewModel
    {
        public string DisplayUrl { get; set; }
        public string Name { get; set; }
        public string CacheUrl { get; set; }
    }
}