using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using DryIoc;
using DryIoc.WebApi;
using Search.Engine.BusinessLogic.Interfaces;
using Search.Engine.BusinessLogic.Services;
using Search.Engine.DataAccess.Interfaces;
using Search.Engine.DataAccess.Repositories;

namespace Search.Engine
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            var c = new Container();
            c.Register<ISearchResultService, SearchResultService>();
            c.Register<IRepository, Repository>(Reuse.Singleton);
            c.Register<IEngineService, EngineService>();


            c.WithWebApi(GlobalConfiguration.Configuration);

            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);

            BundleConfig.RegisterBundles(BundleTable.Bundles);  // Set the dependency resolver to be Autofac.
      

        }
    }
}
