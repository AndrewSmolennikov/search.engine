﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Search.Engine.Data.Constant
{
    public class XmlConstants
    {
        public const string YANDEX_XML = @"<?xml version=""1.0"" encoding=""UTF-8""?>   
                                                 <request>   
                                                      <query>{0}</query>
                                                         <groupings>
                                                              <groupby attr=""d"" 
                                                                  mode=""deep"" 
                                                                  groups-on-page=""10"" 
                                                                  docs-in-group=""1"" />   
                                                         </groupings>   
                                                 </request>";
    }
}