﻿namespace Search.Engine.Data.Constant
{
    public class UrlConstants
    {
        public const string YANDEX_URL = @"https://yandex.com/search/xml?l10n=en&user=asmolennikov&key=03.556095372:f0e06e7c6ee7891f8f6458db3d95b6dd";
        public const string BING_URL = @"https://api.cognitive.microsoft.com/bing/v7.0/search";
    }
}