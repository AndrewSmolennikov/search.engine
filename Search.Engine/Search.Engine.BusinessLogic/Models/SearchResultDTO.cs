﻿
namespace Search.Engine.BusinessLogic.Models
{
    public class SearchResultDTO
    {
        public string DisplayUrl { get; set; }
        public string Name { get; set; }
        public string CacheUrl { get; set; }
    }
}
