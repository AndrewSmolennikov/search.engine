﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Search.Engine.BusinessLogic.Constants;
using Search.Engine.BusinessLogic.Models;
using Search.Engine.Data.Constant;
using Search.Engine.DataAccess;

namespace Search_Engine.Engine
{
    public class BingEngine
    {
        public static Task<List<SearchResultDTO>> SearchAsync(string query)
        {
            return GetBingSearchJsonResultAsync(query).ContinueWith(r =>
            {
                var result = JsonConvert.DeserializeObject<Rootobject>(r.Result);

                return result?.webPages.value.Select(item => new SearchResultDTO
                {
                    Name = item.name,
                    CacheUrl = item.url,
                    DisplayUrl = item.displayUrl
                }).ToList();

            });
        }

        private static Task<string> GetBingSearchJsonResultAsync(string keywords)
        {
            var url = UrlConstants.BING_URL + "?q=" + Uri.EscapeDataString(keywords);

            var request = WebRequest.Create(url);

            request.Headers[HttpRequestHeader.AcceptLanguage] = "es-ES";
            request.Headers["Ocp-Apim-Subscription-Key"] = KeyConstants.BING_KEY;

            return request.GetResponseAsync().ContinueWith((task) => new StreamReader(task.Result.GetResponseStream()).ReadLine());
        }
    }
}

public class Rootobject
{
    public string _type { get; set; }
    public Webpages webPages { get; set; }
    public Rankingresponse rankingResponse { get; set; }
}

public class Webpages
{
    public string webSearchUrl { get; set; }
    public int totalEstimatedMatches { get; set; }
    public Value[] value { get; set; }
}

public class Value
{
    public string id { get; set; }
    public string name { get; set; }
    public string url { get; set; }
    public string displayUrl { get; set; }
    public string snippet { get; set; }
    public DateTime dateLastCrawled { get; set; }
}

public class Rankingresponse
{
    public Mainline mainline { get; set; }
}

public class Mainline
{
    public Item[] items { get; set; }
}

public class Item
{
    public string answerType { get; set; }
    public int resultIndex { get; set; }
    public Value1 value { get; set; }
}

public class Value1
{
    public string id { get; set; }
}
