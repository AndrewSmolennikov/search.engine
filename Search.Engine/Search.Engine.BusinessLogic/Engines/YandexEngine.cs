﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using Search.Engine.BusinessLogic.Models;
using Search.Engine.Data.Constant;

namespace Search.Engine.BusinessLogic.Engines
{
    public class YandexEngine
    {
        public static Task<List<SearchResultDTO>> SearchAsync(string query)
        {
            byte[] bytes = Encoding.UTF8.GetBytes(string.Format(XmlConstants.YANDEX_XML, query));
            HttpWebRequest yandexRequest = (HttpWebRequest)WebRequest.Create(UrlConstants.YANDEX_URL);
            yandexRequest.Method = "POST";
            yandexRequest.ContentLength = bytes.Length;
            yandexRequest.ContentType = "text/xml";

            using (var requestStream = yandexRequest.GetRequestStream())
            {
                requestStream.Write(bytes, 0, bytes.Length);
            }

            return yandexRequest.GetResponseAsync().ContinueWith((task) =>
            {
                XmlReader xmlReader = XmlReader.Create(task.Result.GetResponseStream());
                XDocument xmlResponse = XDocument.Load(xmlReader);

                var groupQuery = from gr in xmlResponse.Elements().
                        Elements("response").
                        Elements("results").
                        Elements("grouping").
                        Elements("group")
                    select gr;

                return ToModelListResult(groupQuery);
            });
            

            
        }

        private static List<SearchResultDTO> ToModelListResult(IEnumerable<XElement> groupQuery)
        {
            var returnResults = new List<SearchResultDTO>();

            var elements = groupQuery as XElement[] ?? groupQuery.ToArray();

            for (var i = 0; i < elements.Length; i++)
            {
                var url = GetValue(elements.ElementAt(i), "url");
                var title = GetValue(elements.ElementAt(i), "title");
                var cacheUrl = GetValue(elements.ElementAt(i), "saved-copy-url");
                returnResults.Add(new SearchResultDTO
                {
                    DisplayUrl = url,
                    CacheUrl = cacheUrl,
                    Name = title
                });
            }

            return returnResults;
        }
        private static string GetValue(XContainer group, string name)
        {
            try
            {
                return group.Element("doc")?.Element(name)?.Value;
            }
            catch
            {
                return string.Empty;
            }
        }
    }
}