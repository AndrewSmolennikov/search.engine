﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Search.Engine.BusinessLogic.Engines;
using Search.Engine.BusinessLogic.Interfaces;
using Search.Engine.BusinessLogic.Models;
using Search.Engine.DataAccess;
using Search.Engine.DataAccess.Interfaces;
using Search_Engine.Engine;

namespace Search.Engine.BusinessLogic.Services
{
    public class SearchResultService : ISearchResultService
    {
        private readonly IRepository _repo;
        private readonly IEngineService _engineService;
        public SearchResultService(IRepository repo, IEngineService engineService)
        {
            _repo = repo;
            _engineService = engineService;
        }
        public IEnumerable<SearchResultDTO> GetResult(string value)
        {
            var requestTasks = _engineService.GetEngines(value);
            var index = Task.WaitAny(requestTasks);
            var requestResults = requestTasks[index].Result;
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<SearchResultDTO, SearchResult>()).CreateMapper();
            var dbMappedResults = mapper.Map<List<SearchResult>>(requestTasks[index].Result);
            _repo.Insert(dbMappedResults);
            return requestResults;

        }

        public IEnumerable<SearchResultDTO> FindExist(string value)
        {
            var foundData = _repo.Find(value);
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<SearchResult, SearchResultDTO>()).CreateMapper();
            var dtoMappedResult = mapper.Map<List<SearchResultDTO>>(foundData.ToList());
            return dtoMappedResult;
        }
    }
}
