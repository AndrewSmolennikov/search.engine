﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Search.Engine.BusinessLogic.Engines;
using Search.Engine.BusinessLogic.Interfaces;
using Search.Engine.BusinessLogic.Models;
using Search_Engine.Engine;

namespace Search.Engine.BusinessLogic.Services
{
    public class EngineService : IEngineService
    {
        public Task<List<SearchResultDTO>>[] GetEngines(string value)
        {
            Task<List<SearchResultDTO>>[] requestTasks = {
                YandexEngine.SearchAsync(value),
                BingEngine.SearchAsync(value)
            };
            return requestTasks;
        }
    }
}
