﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Search.Engine.BusinessLogic.Models;

namespace Search.Engine.BusinessLogic.Interfaces
{
    public interface IEngineService
    {
        Task<List<SearchResultDTO>>[] GetEngines(string value);
    }
}
