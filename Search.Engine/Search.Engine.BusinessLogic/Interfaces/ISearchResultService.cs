﻿using Search.Engine.BusinessLogic.Models;
using System.Collections.Generic;

namespace Search.Engine.BusinessLogic.Interfaces
{
    public  interface ISearchResultService
    {
        IEnumerable<SearchResultDTO> GetResult(string value);
        IEnumerable<SearchResultDTO> FindExist(string value);
    }
}
