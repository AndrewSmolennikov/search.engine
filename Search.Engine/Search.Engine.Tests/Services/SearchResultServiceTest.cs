﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using NSubstitute;
using NUnit.Framework;
using Search.Engine.BusinessLogic.Interfaces;
using Search.Engine.BusinessLogic.Models;
using Search.Engine.BusinessLogic.Services;
using Search.Engine.DataAccess;
using Search.Engine.DataAccess.Interfaces;

namespace Search.Engine.Tests.Services
{
    [TestFixture]
    [Category("BusinessLogicLayerTests")]
    public class SearchResultServiceTest
    {
        [Test]
        public void Should_BeNotEmpty_When_RetrieveResult()
        {
            const string searchvalue = "anyString";
            var expectedItems = new List<SearchResultDTO>
            {
                new SearchResultDTO
                {
                    Name = "name1",
                    CacheUrl = "http:/someAdress1",
                    DisplayUrl = "http:/someAdress1"

                }
            };

            var repo = Substitute.For<IRepository>();
            var engineService = Substitute.For<IEngineService>();

            engineService.GetEngines(searchvalue).Returns(new[] { Task.FromResult(expectedItems) });

            var searchResultService = new SearchResultService(repo, engineService);
            var result = searchResultService.GetResult(searchvalue).ToArray();

            Assert.Multiple(() =>
            {
                Assert.IsNotEmpty(result);
                Assert.AreEqual(result[0].Name, expectedItems[0].Name);
                Assert.AreEqual(result[0].DisplayUrl, expectedItems[0].DisplayUrl);
                Assert.AreEqual(result[0].CacheUrl, expectedItems[0].CacheUrl);
            });
        }

        [Test]
        public void Should_BeNotEmpty_When_RetrieveExistResultFromDataBase()
        {
            const string searchvalue = "anyString";
            var expectedItems = new List<SearchResult>
            {
                new SearchResult
                {
                    Name = searchvalue + "1",
                    CacheUrl = "http:/someAdress1",
                    DisplayUrl = "http:/someAdress1"

                }
            };

            var repo = Substitute.For<IRepository>();
            var engineService = Substitute.For<IEngineService>();

            repo.Find(searchvalue).Returns(expectedItems);
            var searchResultService = new SearchResultService(repo, engineService);
            var result = searchResultService.FindExist(searchvalue).ToArray();

            Assert.Multiple(() =>
            {
                Assert.IsNotEmpty(result);
                Assert.AreEqual(result[0].Name, expectedItems[0].Name);
                Assert.AreEqual(result[0].DisplayUrl, expectedItems[0].DisplayUrl);
                Assert.AreEqual(result[0].CacheUrl, expectedItems[0].CacheUrl);
            });

        }

    }
}
