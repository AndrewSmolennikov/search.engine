﻿using System.Collections.Generic;
using Search.Engine.BusinessLogic.Models;

namespace Search.Engine.Tests.ConstantData
{
    public class SearchResultDTOData
    {
        public static List<SearchResultDTO> GetNotEmptyData()
        {
            return new List<SearchResultDTO>
            {
                new SearchResultDTO
                {
                    Name = "name1",
                    CacheUrl = "http:/someAdress1",
                    DisplayUrl = "http:/someAdress1"

                },
                new SearchResultDTO
                {
                    Name = "name2",
                    CacheUrl = "http:/someAdress2",
                    DisplayUrl = "http:/someAdress2"
                },
                new SearchResultDTO
                {
                    Name = "name3",
                    CacheUrl = "http:/someAdress3",
                    DisplayUrl = "http:/someAdress3"
                }
            };
        }
    }
}
