﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Search.Engine.DataAccess;

namespace Search.Engine.Tests.ConstantData
{
    public class RepositoryData
    {
        public static List<SearchResult> GetNotEmptyData()
        {
            return new List<SearchResult>
            {
                new SearchResult
                {
                    Name = "name1",
                    CacheUrl = "http:/someAdress1",
                    DisplayUrl = "http:/someAdress1"

                },
                new SearchResult
                {
                    Name = "name2",
                    CacheUrl = "http:/someAdress2",
                    DisplayUrl = "http:/someAdress2"
                },
                new SearchResult
                {
                    Name = "name3",
                    CacheUrl = "http:/someAdress3",
                    DisplayUrl = "http:/someAdress3"
                }
            };
        }
    }
}
