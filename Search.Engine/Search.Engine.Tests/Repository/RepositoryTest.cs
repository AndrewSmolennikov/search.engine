﻿using System.Linq;
using NUnit.Framework;
using Search.Engine.Tests.ConstantData;

namespace Search.Engine.Tests.Repository
{
    [TestFixture]
    [Category("DataAccessLayerTests")]
    public class RepositoryTest
    {
        [Test]
        public void Should_InsertCorrectly_When_InsertingData()
        {
            var repository = new DataAccess.Repositories.Repository();
            var items = RepositoryData.GetNotEmptyData();
            var allBefore = repository.GetAll();
            repository.Insert(items);
            var total = allBefore.Concat(items).ToList();
            var allAfter = repository.GetAll();
            Assert.AreEqual(total, allAfter);
        }

        [Test]
        public void Should_ReturnValue_When_PerformSearch()
        {
            const string searchValue = "Name1";
            var repository = new DataAccess.Repositories.Repository();
            var items = RepositoryData.GetNotEmptyData();
            repository.Insert(items);
            var results = repository.Find(searchValue);
            var isExist = results.Count(x => x.Name.ToLower().Contains(searchValue.ToLower())) > 0;
            Assert.True(isExist);
        }
    }
}
