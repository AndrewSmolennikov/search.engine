﻿using System.Collections.Generic;
using System.Linq;
using FizzWare.NBuilder;
using NSubstitute;
using NUnit.Framework;
using Search.Engine.BusinessLogic.Interfaces;
using Search.Engine.BusinessLogic.Models;
using Search.Engine.Controllers;
using Search.Engine.Models;

namespace Search.Engine.Tests.Controllers
{
    [TestFixture]
    [Category("ResultControllerTests")]
    public class ResultControllerTest
    {
        [Test]
        public void Should_ReturnNotEmpty_When_ProceedSearchValue()
        {
            const string value = "anyValue";
            var searchResultService = Substitute.For<ISearchResultService>();
            var expectedItems = new List<SearchResultDTO>
            {
                new SearchResultDTO
                {
                    Name = value + "1",
                    CacheUrl = "http:/" + value + "1",
                    DisplayUrl = "http:/" + value + "1"
                }
            };
            searchResultService.GetResult(value).Returns(expectedItems);

            ResultController controller = new ResultController(searchResultService);
            var result = controller.Get(value).ToList();
            Assert.Multiple(() =>
            {
                Assert.True(result.Any());
                Assert.AreEqual(result[0].Name, expectedItems[0].Name);
                Assert.AreEqual(result[0].DisplayUrl, expectedItems[0].DisplayUrl);
                Assert.AreEqual(result[0].CacheUrl, expectedItems[0].CacheUrl);
            });

        }

        [Test]
        public void Should_CalledFindExist_When_ExistInDatabase()
        {
            const string value = "anyValue";
            var searchResultService = Substitute.For<ISearchResultService>();
            var expectedItems = new List<SearchResultDTO>
            {
                new SearchResultDTO
                {
                    Name = value + "1",
                    CacheUrl = "http:/" + value + "1",
                    DisplayUrl = "http:/" + value + "1"
                }
            };

            searchResultService.FindExist(value).Returns(expectedItems);

            ResultController controller = new ResultController(searchResultService);
            var result = controller.Get(value).ToList();

            searchResultService.Received().FindExist(value);
            searchResultService.DidNotReceive().GetResult(value);

            Assert.Multiple(() =>
            {
                Assert.True(result.Any());
                Assert.AreEqual(result[0].Name, expectedItems[0].Name);
                Assert.AreEqual(result[0].DisplayUrl, expectedItems[0].DisplayUrl);
                Assert.AreEqual(result[0].CacheUrl, expectedItems[0].CacheUrl);
            });
        }

        [Test]
        public void Should_CalledGetResult_When_NotExistInDatabase()
        {
            const string value = "anyValue";
            var searchResultService = Substitute.For<ISearchResultService>();
            var expectedItems = new List<SearchResultDTO>
            {
                new SearchResultDTO
                {
                    Name = "notAnyValue1",
                    CacheUrl = "http:/notAnyValue1",
                    DisplayUrl = "http:/notAnyValue1"
                }
            };

            searchResultService.GetResult(value).Returns(expectedItems);

            ResultController controller = new ResultController(searchResultService);
            var result = controller.Get(value).ToList();

            searchResultService.Received().GetResult(value);
            searchResultService.Received().FindExist(value);

            Assert.Multiple(() =>
            {
                Assert.True(result.Any());
                Assert.AreEqual(result[0].Name, expectedItems[0].Name);
                Assert.AreEqual(result[0].DisplayUrl, expectedItems[0].DisplayUrl);
                Assert.AreEqual(result[0].CacheUrl, expectedItems[0].CacheUrl);
            });
        }

        [Test]
        public void Should_ReturnEmpty_When_ProceedEmptySearchValue()
        {
            var value = string.Empty;
            var searchResultService = Substitute.For<ISearchResultService>();
            ResultController controller = new ResultController(searchResultService);
            var result = controller.Get(value);
            Assert.False(result.Any());
        }

        [Test]
        public void Should_ReturnEmpty_When_ProceedEmptySearchExistValue()
        {
            var value = string.Empty;
            var searchResultService = Substitute.For<ISearchResultService>();
            ResultController controller = new ResultController(searchResultService);
            var result = controller.GetExist(value);
            Assert.False(result.Any());
        }

        [Test]
        public void Should_ReturnNotEmpty_When_ExistInDataBase()
        {
            const string value = "anyValue";
            var searchResultService = Substitute.For<ISearchResultService>();
            var expectedItems = new List<SearchResultDTO>
            {
                new SearchResultDTO
                {
                    Name = value + "1",
                    CacheUrl = "http:/" + value + "1",
                    DisplayUrl = "http:/" + value + "1"
                }
            };
            searchResultService.FindExist(value).Returns(expectedItems);

            ResultController controller = new ResultController(searchResultService);
            var result = controller.GetExist(value).ToList();

            Assert.Multiple(() =>
            {
                Assert.True(result.Any());
                Assert.AreEqual(result[0].Name, expectedItems[0].Name);
                Assert.AreEqual(result[0].DisplayUrl, expectedItems[0].DisplayUrl);
                Assert.AreEqual(result[0].CacheUrl, expectedItems[0].CacheUrl);
            });
        }

    }
}
