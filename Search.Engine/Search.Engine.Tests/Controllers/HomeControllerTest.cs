﻿using System.Web.Mvc;
using NUnit.Framework;
using Search.Engine.Controllers;

namespace Search.Engine.Tests.Controllers
{
    [TestFixture]
    [Category("HomeControllerTests")]
    public class HomeControllerTest
    {
        [Test]
        public void Should_ReturnIndex_When_AppStarts()
        {
            HomeController controller = new HomeController();

            FilePathResult result = controller.Index() as FilePathResult;

            Assert.Multiple(() =>
            {
                Assert.IsNotNull(result);
                Assert.AreEqual("~/dist/index.html", result.FileName);
            });


        }
    }
}
