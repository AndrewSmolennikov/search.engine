﻿using System.Collections.Generic;


namespace Search.Engine.DataAccess.Interfaces
{
    public interface IRepository
    {
        void Insert(List<SearchResult> data);
        IEnumerable<SearchResult> Find(string value);
        IEnumerable<SearchResult> GetAll();
    }
}
