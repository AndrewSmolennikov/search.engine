﻿using System;
using System.Collections.Generic;
using System.Linq;
using Search.Engine.DataAccess.Interfaces;

namespace Search.Engine.DataAccess.Repositories
{
    public class Repository : IRepository
    {
        private List<SearchResult> _repository;

        public Repository()
        {
            _repository = new List<SearchResult>();
        }

        public void Insert(List<SearchResult> data)
        {
            _repository.AddRange(data);
        }

        public IEnumerable<SearchResult> Find(string value)
        {
            return _repository.Where(x => x.Name.ToLower().Contains(value.Trim().ToLower())).ToArray();
        }

        public IEnumerable<SearchResult> GetAll()
        {
            return _repository.ToArray();
        }
    }
}
