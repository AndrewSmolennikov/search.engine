﻿
namespace Search.Engine.DataAccess
{
    public class SearchResult
    {
        public string DisplayUrl { get; set; }
        public string Name { get; set; }
        public string CacheUrl { get; set; }
    }
}
